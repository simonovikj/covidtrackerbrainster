<?php

// include_once "../functions/worldCaseInsert";
include_once "../functions/connectDB.php";
include_once "../functions/WorldCaseFromDB.php";


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>COVID 19 Tracker</title>
    <link rel="stylesheet" href="style/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="image/jpg" href="img/favicon.png" />
</head>

<body>
<nav class="navbar navbar-expand-md navbar-light bg-light">
        <a class="navbar-brand" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public">Covid19 Tracker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public/dataByCountry.php">Covid Data by Country</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public/sync.php">Data Sync</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-md text-center bg-light fullHeight">
        <h1 class="py-5 font-weight-bold headingColor">Covid 19 Tracker</h1>
        <p>Please press the synchronize button to download today's coronavirus data. <br> After click please go back to
            the first page to see the data. <br> Have a nice day :)</p>
        <p class="pt-4 text-secondary">The data was last updated on: <?php echo "$date" ?></p>
        <a href="http://localhost/COVID19Tracker/covidtrackerbrainster/functions/dataInsertDB.php"
            class="btn btn-success font-weight-bold my-5 p-3">Synchronize Last Data</a>
    </div>

</body>

</html>