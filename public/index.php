<?php 

include_once "../functions/WorldCaseFromDB.php";
include_once "../functions/dateTime.php";
include_once "../functions/printMessages.php";

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>COVID 19 Tracker</title>
    <link rel="stylesheet" href="style/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="image/jpg" href="img/favicon.png"/>
</head>

<body >
    <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <a class="navbar-brand" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public">Covid19 Tracker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public/dataByCountry.php">Covid Data by Country</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public/sync.php">Data Sync</a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<main>
    <div class="container-md text-center py-4 bg-white fullHeight">
        <?php 
        printErrorMessages();
        printSuccessMessages();
        ?>
        <h1 class="mt-4 font-weight-bold headingColor">Covid19 Tracker World</h1>
        <p class="text-secondary">Central European Time.. <br> Today is: <?php echo $time ?></p>

        

        <div class="py-5">
            <div class="py-3">
                <h3>Total Cases:</h3>
                <p class="totalNumberColor numberFirstPage">
                    <?php echo $totalCase?>
                </p>
            </div> 
            <div class="py-3">
                <h3>Recovered:</h3>
                <p class="recoveredNumberColor numberFirstPage">
                    <?php 
                        echo $recoveredCase;
                    ?>
                </p>
            </div>
            <div class="py-3">
                <h3>Active:</h3>
                <p class="activeNumberColor numberFirstPage">
                    <?php 
                       activeCase($totalCase, $recoveredCase, $deathsCase);
                    ?>
                </p>
            </div>
            <div class="py-3">
                <h3>Deaths:</h3>
                <p class="deathsColor numberFirstPage">
                    <?php 
                        echo $deathsCase;
                    ?>
                </p>
            </div>
            
        </div>

 


        
    </div>

</main>

</body>

</html>