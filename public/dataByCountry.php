<?php


include_once "../functions/connectDB.php";
include_once "../functions/WorldCaseFromDB.php";
include_once "../functions/dateTime.php";
// include_once "../functions/CountryName.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>COVID 19 Tracker</title>
    <link rel="stylesheet" href="style/style.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="image/jpg" href="img/favicon.png" />
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <a class="navbar-brand" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public">Covid19 Tracker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="http://localhost/COVID19Tracker/covidtrackerbrainster/public">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                        href="http://localhost/COVID19Tracker/covidtrackerbrainster/public/dataByCountry.php">Covid Data
                        by Country</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                        href="http://localhost/COVID19Tracker/covidtrackerbrainster/public/sync.php">Data Sync</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-md text-center bg-light fullHeight">
        <h1 class="py-5 font-weight-bold headingColor">Covid 19 Tracker by Country</h1>
        <p>Select a country for which you want to see today's coronavirus cases...</p>
        <p class=" text-secondary">The data was last updated on: <?php echo "$date" ?></p>
        <form class="py-5">
            <select name="Countries[]" id="" class="px-5 py-2">
                <option value="NoSelected">Select Country..</option>
                <?php 
            
            $countries = "SELECT CountriName, totalConfirmed FROM Countries LIMIT 190";
            $stmt = $connect->query($countries);
            while($country = $stmt->fetch()){
                
                echo "<option value='" . $country['CountriName'] . "'>" . $country['CountriName'] . "</option> </br>";
            }
        
            ?>
            </select>
            <input type="submit" class="btn btn-info px-5 py-1 mx-2" value="Data info">

        </form>



        <div class="table pt-4">
            <div class="table-wrapper">
                <!-- <h2>TABELA</h2> -->
                <div>
                    <div class="b-table-sticky-header table-responsive" style="max-height: 100%">
                        <table stickycolumn="" role="table" aria-busy="false" aria-colcount="458"
                            class="table b-table table-hover table-bordered border" id="__BVID__36">

                            <thead role="rowgroup" class>
                                <tr role="row" class>

                                    <?php 

                                    if(!empty($_GET['Countries'])) {

                                        foreach($_GET['Countries'] as $selected) {
                                            $query = "SELECT * FROM `Countries` WHERE CountriName='$selected' ORDER BY insertDate DESC ";
                                            $stmt = $connect->query($query);
                                            $data = $stmt->fetch();

                                            if($selected == "NoSelected") {
                                                echo "<h4>You selected data for: </h4>";
                                                echo "<h2 class='selectedData'> World </h2><br>" ;
                                                echo "<p>Total Confirmed : </p><h4>" . $totalCase . "</h4> <br>";
                                                echo "<p>Total Recovered : </p><h4>" . $recoveredCase . "</h4> <br>";
                                                echo "<p>Total Deaths : </p><h4>" . $deathsCase . "</h4> <br>";
                                            } else {
                                                echo "<h4>You selected data for:  </h4>";
                                                echo "<h2 class='selectedData'>$selected</h2><br>";
                                                echo " <th role='columnheader' scope='col' tabindex='0' aria-colindex='1' aria-label=' ' aria-sort='descending' class='table-b-table-default b-table-sticky-column'><div class='px-5'>$selected </div></th>";
                                                // echo "<td roll='cell' aria-colindex='1' class='table-b-table-default b-table-sticky-column px-5'> $selected</td>";
                                                echo "<td roll='cell' aria-colindex='1' class='table-b-table-default b-table-sticky-column px-3'> Total Confirmed </td>";
                                                echo "<td roll='cell' aria-colindex='1' class='table-b-table-default b-table-sticky-column px-3'> Total Recovered </td>";
                                                echo "<td roll='cell' aria-colindex='1' class='table-b-table-default b-table-sticky-column px-3'> Total Deaths </td>";
                                                echo "<td roll='cell' aria-colindex='1' class='table-b-table-default b-table-sticky-column px-3'> New Confirmed </td>";
                                                echo "<td roll='cell' aria-colindex='1' class='table-b-table-default b-table-sticky-column px-3'> New Recovered </td>";
                                                echo "<td roll='cell' aria-colindex='1' class='table-b-table-default b-table-sticky-column px-3'> New Deaths </td>";

                                            }
                                        }
                                    }                        
                    
                                    echo " </tr>";
                                    echo "</thead>";
                                    echo "<tbody role='row' class>";
                                    echo "<tr>";
               
                
                
                        $time = $connect->query($query);

                        while($date = $time->fetch()) {
                            $dateTable = $date['insertDate'];
                            $date = $date['insertDate'];
                            $date = strtotime($date);
                            $date = date("d/m/Y", $date);  
                            echo "<th role='columnheader' scope='col' tabindex='0' aria-colindex=' ' aria-sort='none' class='table-b-table-default'><div>$date</div></th>";

                            $queryTime = "SELECT * FROM `Countries` WHERE CountriName = '$selected' AND insertDate = '$dateTable'";
                            $podatci = $connect->query($queryTime);
                            $podatci1 = $podatci->fetch();

                            echo "<td>{$podatci1['totalConfirmed']}</td>";
                            echo "<td>{$podatci1['totalRecovered']}</td>";
                            echo "<td>{$podatci1['totalDeaths']}</td>";
                            echo "<td>{$podatci1['newConfirmed']}</td>";
                            echo "<td>{$podatci1['newRecovered']}</td>";
                            echo "<td>{$podatci1['newDeaths']}</td>";
                            echo "</tr>";
                        }
                ?>

                                </tr>
                                </tbody>

                        </table>


                    </div>

                        
</body>

</html>