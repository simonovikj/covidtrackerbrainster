<?php



function printErrorMessages() {
    $errorMessages = [
    'general' => "An error has occurred on the site at this time, please try again later. ",
    'alredy-download' => "You didn't have downloaded the new data, please try again later."
    ];



    if(isset($_GET['status']) && $_GET['status'] == "error") {
        $msg = $errorMessages['general'];
        if (isset($errorMessages[$_GET['reason']])) {
                $msg = $errorMessages[$_GET['reason']];
        }

        echo "<p class='text-danger strong'>$msg</p>";
    }
}


function printSuccessMessages() {
    $succesMessage = [
        'general' => "Have a nice day",
        'success-downloaded' => "You have successfully downloaded the new data"
    ];

    if(isset($_GET['status']) && $_GET['status'] == "success") {
        $msg = $succesMessage['general'];
        if (isset($succesMessage[$_GET['reason']])) {
                $msg = $succesMessage[$_GET['reason']];
        }

        
     echo "<p class='text-success strong'>$msg</p>";
    }
}



        